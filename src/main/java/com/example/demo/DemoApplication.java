package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {
    int x,y;

    @GetMapping("/")
    public String home() {
        try {
            // Some code that may throw an exception
        } catch (Exception e) {
            // Empty catch block to trigger a warning
        }
        return "Spring is here!";
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
